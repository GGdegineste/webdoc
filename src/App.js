import React, { Component } from 'react';
import './App.css';
import AnimationPage from './Component/AnimationPage';
import TrailerPage from './Component/TrailerPage';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {skipIntro: false};
    this.handleClick = this.handleClick.bind(this) //Remettre dans le contexte 
  }

  handleClick() {
    this.setState({ skipIntro: true });
    
  }

  render () {
    const isSkip = this.state.skipIntro
    return (
      !isSkip ? <TrailerPage skip={this.handleClick}/> : <AnimationPage/>
    );
}
}
export default App
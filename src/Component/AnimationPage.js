import React, { Component, useState } from 'react';
import '../App.css';
import Moon from './Video/Moon.mp4';
import Menu from './Menu';

function AnimationPage(){
    const [isToggled, setIsToggled] = useState(false);

    const toggleHandler = () => {
      setIsToggled(!isToggled)
    };

  return (
    <div className='App'>
      <video autoPlay muted mut
      style={{
        position: "absolute",
        width: "100%",
        left: "50%",
        top: "50%",
        height: "100%",
        objectFit: "cover",
        transform: "translate(-50%, -50%)",
        zIndex: "-1"
      }}
      >
        <source src={Moon} type="video/mp4" />
      </video>
      <Menu toggled={isToggled}/>
      <button onClick={toggleHandler}> MENU </button>
    </div>
    
  )
}
export default AnimationPage
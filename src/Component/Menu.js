import React from "react";
import { animated, useSpring } from "react-spring/renderprops";

const Menu = (props)  => {
    
    var { x } = useSpring({ x: props.toggled ? 0 : 100 });
    return(
    <animated.div className="nav_container"
        >
        <nav className="nav">
            <a href="#" >Accueil</a>
            <a href="#">Fil Actu</a>
            <a href="#">A propos</a>
        </nav>
    </animated.div>);

    }



export default Menu;
const nav_container = {
    position: "fixed",
    top: "0",
    bottom: "0",
    right: "70%",
    left: "0",
    background: "#008cba"
}

const nav = {
    width: "100%",
    height: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
}
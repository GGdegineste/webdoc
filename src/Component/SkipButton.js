import React from 'react';
import  { Spring }  from 'react-spring/renderprops';

function SkipButton({skip}) {
    return (
        <Spring
            from={{ opacity: 0}}
            to={{ opacity: 1}}
            config={{ delay: 3000, duration: 1000}}
        >
            { props => (
                <div style={props}>
                    <div> 
                        <button
                            onClick={skip}
                            >Passer au WebDoc >
                        </button>
                    </div>
                    
                </div>
            )}
        </Spring>
        
    )
}

export default SkipButton;
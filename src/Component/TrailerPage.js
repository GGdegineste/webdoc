import React, { Component } from 'react';
import '../App.css';
import AnimationPage from './AnimationPage';
import Projet_Trailer_Webdoc_Compresse from './Video/Projet_Trailer_Webdoc_Compresse.mp4';
import SkipButton from './SkipButton';


function TrailerPage({skip}) {
    return  (
            <div className='App'>
                <video autoPlay loop
                    style={{
                        position: "absolute",
                        width: "80%",
                        left: "50%",
                        top: "50%",
                        height: "80%",
                        objectFit: "cover",
                        transform: "translate(-50%, -50%)",
                        zIndex: "-1"
                    }}
                    >
                    <source src={Projet_Trailer_Webdoc_Compresse} type="video/mp4" />
                </video>
                <SkipButton skip={skip}/>
            </div>
            
            )
}

export default TrailerPage